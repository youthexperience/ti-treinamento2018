# Treinamento TI

## Como baixar o repositório

* Baixar algum cliente Git (recomendo o [GitBash](https://gitforwindows.org)). Você pode baixar algum cliente Git com interface visual. Mas eu usarei Git na linha de comando nos tutoriais, por isso recomendo o Git Bash
* Abrir o Git Bash na pasta que você quer instalar o repositório
* Dar clone no repositório. Digite na shell "git clone https://bitbucket.org/youthexperience/ti-treinamento2018.git" (sem as aspas). Provavelmente isso vai pedir o seu usuário e senha do BitBucket.
* Caso você já tenha clonado o repositório antes, e apenas queira atualizá-lo, digite "git pull".

## Como dar commit em algum código que você fez

* Você produz e salva o código nessa pasta que você acabou de criar. Esse código salvo não atualiza automaticamente o código no repositório online (que todos tem acesso). Para atualizar o repositório online, é necessário fazer uso do comando commit. Primeiro salve todas as modificações que você fez no seu projeto.
* Certifique que o GitBash está aberto dentro da pasta do seu repositório. Para conferir, é só olhar se a pasta que você está contém uma pasta .git
* O repositório tem vários branches. Cada branch serve para segmentar o projeto em várias vertentes. Normalmente a principal branch é a master. Para acostumar vocês com o control, eu deixei cada um de vocês somente com permissão de dar commit em sua própria branch. Para mudar pra sua branch, digite o comando "git checkout $nomeDaBranch$", substituindo $nomeDaBranch$ pelo nome adequado.
* Fale para o git adicionar suas modificações nos arquivos no path dele. Comando "git add .".
* Dê o commit. Um commit basicamente é um ponto que é criado na árvore de modificações do git. Ele vê quais modificações você fez no repositório e armazena as mudanças. Ele também deve ser feito com uma mensagem que você explica as mudanças que você fez de forma curta e descritiva. Comando "git commit -m "Digite a mensagem aqui"".
* Pronto. Você fez o commit no seu repositório git no seu PC. Para fazer com que o commit seja recebido pelo repositório git no servidor, digite o comando "git push". Ele provavelmente vai pedir seu usuário e senha do bitbucket.

## Como fazer a questão 1 do dever

### Setup
* Instalar o XAMPP, Sublime, o Git Bash e realizar os passos acima para dar clone no repositório
* Ligue o Apache e o MySQL no XAMPP
* Vá em http://localhost/phpmyadmin para abrir o administrador do Banco de Dados. Crie um database com o nome "ti-tutorial". Entre nesse database. Vá em Import>EscolherArquivo e escolha o arquivo ti-tutorial.sql que veio no repositório. Executar. Você criou o database :)
* Crie uma pasta em C:/xampp/htdocs com o nome "tutorial-ti" e coloque nela os arquivos que estão dentro da pasta do repositório.
* Teste se está funcionando. Vá em http://localhost/tutorial-ti e deve aparecer a mensagem Index.

### Desenvolvimento
#### Criando um jeito de coletar posts de alguém na dashboard
* No controller Dashboard, crie um método para colher o post de alguém. Esse método pode ser:
````php
function newPost()
{
	... código aqui. O nome do método pode ser qualquer um. Esse meu foi só uma sugestão. Os nomes dos métodos tem que ser bem sugestivos do que eles fazem
}
````
* Nesse método, coloque no começo dele uma maneira de impedir que usuários não logados consigam acessá-lo. O código já existe no método index() do mesmo controller
* Crie, na pasta views/dashboard, um arquivo newPost.php. O nome do arquivo deve ser igual ao do método que você escreveu. Essa será a view. Coloque nesse arquivo algum texto pra saber quando ele abriu.
* No método newPost() no controller, peça para renderizar a view. A função usada para render já está pronta. Só olhar o exemplo no método index(). Teste se está funcionando indo para http://localhost/ti-tutorial/dashboard/newPost
* Já temos uma página funcionando. Mas ela ainda não está funcional e não colhe nenhum dado. Em views/dashboard/newPost.php, devemos criar um HTML que consiga colher a informação. Crie um form que pegue o título e o conteúdo do post que o autor quer fazer. Esse form deve ter um botão de submit e deve ter os atributos method="POST" e action="". O título deve ter atributo name="title-post" e o conteúdo deve ter atributo name="content-post". Algo parecido foi implementado em views/login/index.php. Não se esqueça de colocar <input type="hidden" name="form-submitted"/> no seu form
* Com a view pronta, devemos coletar as informações que serão mandadas para ela no controller. No método newPost(), faça algo parecido com o método index (do controller login). Cheque se o form foi submetido e depois pegue as variáveis recebidas na variável $_POST (nesse caso seria $_POST['title-post'] e $_POST[content-post]). Print elas na tela com o comando echo e teste a página no navegador para ver se está funcionando.
* Agora que vimos que conseguimos acessar as variáveis no controller, precisamos criar um Model que armazene essas variáveis no BD, assim como fizemos no caso do login. Primeiramente, precisamos criar uma tabela no BD para armazenar os posts. Acesse o BD em http://localhost/phpmyadmin e crie uma tabela com o nome "posts". Ela deve ter as colunas 'id', 'title', 'content', e outras que julgar necessário. O nome das colunas é uma sugestão.
* Após criado o BD, vá no arquivo models/dashboard_model.php. Nele, você deve criar um método que recebe o post do controller e armazena ele no BD. Crie um método como esse:
````php
public function saveNewPost($title, $content)
{
		...coloque o código aqui. Nomes são sugestões
}
````
* Nesse método, você deve pegar as variáveis $title e $content que serão informadas pelo controller e armazenar na tabela que você criou no DB. Um modelo de como fazer isso está no arquivo login_model.php. A diferença, que ao invés de usar um comando SQL de SELECT, você deve usar um comando INSERT. Qualquer dúvida sobre esse comandos, tem muita coisa na internet. Recomendo o [W3Schools](https://www.w3schools.com/sql/sql_insert.asp).
* Depois de terminar de contruir esse método no model, vá para o controller Dashboard. Agora você tem que chamar essa função do model e passar para ela os parâmetros necessários. Um exemplo disso está no controller Login.
* Teste isso agora. Vá em http://localhost/ti-tutorial/dashboard/newPost, faça um novo post, clique no botão de submit. Depois vá em http://localhost/phpmyadmin, vá na tabela d eposts e veja se o post está lá.
* Sucesso!

#### Mostrando isso para as pessoas na index
* Agora já temos os post armazenados no DB. Precisamos pegar esses post no controller index e mostrá-los para as pessoas. O controller index tem um método index, que está assim:
````php
function index()
{
	$this->render('index/index');
}
````
* Temos que escrever um model que consulte os post no DB e dê para o controller index conseguir usá-los. Vá na pasta models e crie o arquivo index_model.php. Copie a forma que os outros models estão, como o nome de classe padronizado, o construtor, a herança etc.
* Agora crie um método para pesquisar os posts no DB. Pode ser assim:
````php
public function getAllPosts()
{
		...coloque o código aqui. Nomes são sugestões
}
````
* Nesse método, você deve fazer uma consulta no BD com o comando SELECT. Armazenar esse resultados num vetor e retornar esse vetor. retornando o vetor, o controller consegue saber quais posts existem. Um exemplo disso está no método index do dashboard_model.
* Após criar esse método no model. Chame ele no controller index e armazene o resultado de uma forma que a view consiga acessar. Um exemplo disso está no método index do dashboard controller.
* Depois disso, vá em views/index/index.php acesse o vetor que foi informado no controller e printe ele através do comando echo. Um exemplo disso está em views/dashboard/index.php
* Teste agora em http://localhost/ti-tutorial/index. Veja se os posts estão aparecendo.

#### Como deixar mais bonito
* Adicione arquivos .css que deixem a página mais atrativa. Tem alguns tutoriais disso na NET. O control usa um template que já tem .css prontos, então não devemos usar muito isso durante o ano. Mas é bom treinar um pouco de CSS porque isso ajuda na sua habilidade de desenvolver.

####Por último
* Dê commit no seu código, copiando as mudanças pra pasta em que você deu clone e dando commit como eu já mostrei nas intruções acima
