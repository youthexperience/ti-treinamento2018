<?php 


/**
* 
*/
class Dashboard extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		Session::init();
		if(!Session::get('loggedIn') === True)
		{
			Controller::redirect(LOGIN_LINK);
		}

		require_once 'models/dashboard_model.php';
		$model = new DashboardModel();

		$this->name = $model->getNameFromId(Session::get('userId'));

		$this->render('dashboard/index');
	}

	function logout()
	{
		Session::init();
		Session::destroy();
		Controller::redirect(LOGIN_LINK);
	}
}