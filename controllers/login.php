<?php 


/**
* 
*/
class Login extends Controller
{
	
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		Session::init();
		if(Session::get('loggedIn'))
		{
			Controller::redirect(DASHBOARD_LINK);
		}


		if(isset($_POST['form-submitted']))
		{
			require_once 'models/login_model.php';
			$model = new LoginModel();

			$result = $model->login($_POST['username'], $_POST['password']);

			if (!$result === false)
			{
				Session::set('loggedIn', true);
				Session::set('userId', $result);

				Controller::redirect(DASHBOARD_LINK);
			}
		}

		$this->render('login/index');
	}
}

?>