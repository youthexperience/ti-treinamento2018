<?php

/**
* 
*/
abstract class Controller
{
	
	protected function __construct()
	{
	}

	protected function render($view)
	{
		require_once 'views/'.$view.'.php';
	}

	protected function redirect ($location) {
        header('Location: '.$location);
        exit;
    }
}

?>