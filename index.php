<?php

require_once 'controllers/controller.php';
require_once 'models/model.php';
require_once 'models/session.php';
require_once 'config/database.php';
require_once 'config/paths.php';


if(!isset($_GET['url'])) 
{
	$url = 'index';
}
else{
	$url = $_GET['url']	;
}


$url = rtrim($url, '/');
$url = explode('/', $url);

if (!isset($url[0]))
{
	$url[0] = 'index';
}

$controllerName = $url[0];
$controllerFile = 'controllers/'.$controllerName.'.php';

if(file_exists($controllerFile))
{
	require_once $controllerFile;

	$controller = new $controllerName();

	if(isset($url[1]))
	{
		$method = $url[1];
	}
	else
	{
		$method = 'index';
	}

	if(method_exists($controller, $method))
	{
		if (isset($url[2]))
		{
			$controller->$method($url[2]);
		}
		else
		{
			$controller->$method();	
		}
	}
	else{
		error();
	}

}
else
{
	error();
}

function error()
{
	echo 'Esse é um erro';
}

?>