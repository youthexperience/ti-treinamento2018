<?php 

/**
* 
*/
class DashboardModel extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	function getNameFromId($id)
	{
		try
		{
			$conn = Service::openDb();

			$stmt = $conn->prepare("SELECT name FROM user WHERE id = :id");
			$stmt->execute(array(':id' => $id));

			$user = $stmt->fetch();


			return $user['name'];
		}
		catch (PDOException $e)
		{
			echo 'Erro na Query';
			die();
			return false;
		}
		finally{
			Service::closeDb();
		}
	}
}

?>