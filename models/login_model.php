<?php 

/**
* 
*/
class LoginModel extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function login($username, $password)
	{
		try
		{
			$conn = Service::openDb();

			$stmt = $conn->prepare("SELECT * FROM user WHERE username = :username");
			$stmt->execute(array(':username' => $username));

			$user = $stmt->fetch();

			if ($password == $user['password'])
			{
				return $user['id'];
			}

			return false;
		}
		catch (PDOException $e)
		{
			echo $e->getMessage();
			die();
			return false;
		}
		finally{
			Service::closeDb();
		}
	}
}

?>